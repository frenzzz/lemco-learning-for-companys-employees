@extends('layouts.dashboard-layout')

@section('title')
<title>LEMCO panel</title>
@endsection

@section('head')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('content')

<section class="body">

    @include('components.dashboard-header')
    
    <div class="inner-wrapper">
        
        @include('components.dashboard-navbar')

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Corsi</h2>
            </header>

            <div class="row">
				<div class="col-lg-12">
                    <section class="panel">
                       <form class="form-horizontal form-bordered" method="post" action="{{ url('request/new-course') }}?ref={{$_SERVER['REQUEST_URI']}}" enctype="multipart/form-data">
                            @csrf
                            <div class="panel-body">

                               <div class="form-group">
                                    <label class="col-sm-3 control-label">Nome del nuovo corso <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="course[title]" class="form-control" placeholder="eg.: John Doe" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Descrizione</label>
                                    <div class="col-sm-9">
                                        <textarea name="course[description]" rows="5" class="form-control" placeholder="Descrivi il corso, come è articolato, ecc"></textarea>
                                    </div>
                                </div>
                            </div>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button class="btn btn-success">Salva</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>
                            </footer>
                        </form>

                            <div class="row">
                                <div class="col-sm-6">
                                    <hr>
                                </div>
                            </div>
                            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th style="width: 20%">Create at</th>
                                        <th style="width: 10%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{!$courses = App\Course::where('id_store', Auth::user()->id_store)->get()}}
                                    @foreach($courses as $course)
                                    <tr class="gradeX">
                                        <td>{{ $course->title }}</td>
                                        <td>{{ $course->description }}</td>
                                        <td>{{ $course->created_at }}</td>
                                        <td class="actions">
                                            <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                                            <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                                            <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ url('request/delete-course').'/'.$course->id }}?ref={{$_SERVER['REQUEST_URI']}}" class="on-default remove*row" onclick="return confirm(‘Confermi la cancellazione’);"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </section>
                </div>
            </div>
        </section>

    </div>
</section>

<div id="dialog" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Are you sure?</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                    <p>Are you sure that you want to delete this row?</p>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button id="dialogConfirm" class="btn btn-primary">Confirm</button>
                    <button id="dialogCancel" class="btn btn-default">Cancel</button>
                </div>
            </div>
        </footer>
    </section>
</div>

<!-- Vendor -->
<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

<!-- Specific Page Vendor -->
<script src="{{ asset('/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('/js/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('/js/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('/js/theme.init.js') }}"></script>


<!-- Examples -->
<script src="{{ asset('/js/tables/examples.datatables.editable.js') }}"></script>

@endsection