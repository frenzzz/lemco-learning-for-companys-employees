@extends('layouts.sign-layout')

@section('title')
<title>LEMCO panel</title>
@endsection

@section('content')
<!-- start: page -->
<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="{{ asset('/images/logo.png') }}" height="54" alt="Porto Admin" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<form action="{{ url('register-admin') }}" method="post">
                        @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Name and surname</label>
                                        <input type="text" name="name_surname" class="form-control
                                        {{ $errors->has('name_surname') ? ' is-invalid' : '' }}" 
                                        value="{{ old('name_surname') }}" required>
                                        @if ($errors->has('name_surname'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name_surname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Company name</label>
                                        <input type="text" name="company_name" class="form-control
                                        {{ $errors->has('company_name') ? ' is-invalid' : '' }}" 
                                        value="{{ old('company_name') }}" required>
                                        @if ($errors->has('company_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" 
                                        value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Street</label>
                                        <input type="text" name="company_street" class="form-control
                                        {{ $errors->has('company_street') ? ' is-invalid' : '' }}" 
                                        value="{{ old('company_street') }}" required>
                                        @if ($errors->has('company_street'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('company_street') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <input type="text" name="phone" class="form-control 
                                        {{ $errors->has('phone') ? ' is-invalid' : '' }}" 
                                        value="{{ old('phone') }}" required>
                                        @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input type="text" name="company_city" class="form-control 
                                        {{ $errors->has('company_city') ? ' is-invalid' : '' }}" 
                                        value="{{ old('company_city') }}" required>
                                        @if ($errors->has('company_city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('company_city') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
										<label class="control-label">Password</label>
										<input name="password" type="password" class="form-control 
                                        {{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                        value="{{ old('password') }}" required>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
									</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">CAP</label>
                                        <input type="text" name="company_cap" class="form-control 
                                        {{ $errors->has('company_cap') ? ' is-invalid' : '' }}" 
                                        value="{{ old('company_cap') }}" required>
                                        @if ($errors->has('company_cap'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('company_cap') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Password Confirmation</label>
										<input name="password_confirm" type="password" class="form-control 
                                        {{ $errors->has('password_confirm') ? ' is-invalid' : '' }}" 
                                        value="{{ old('password_confirm') }}" required>
                                        @if ($errors->has('password_confirm'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirm') }}</strong>
                                        </span>
                                        @endif
									</div>
                                </div>
                            </div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="AgreeTerms" name="agreeterms" type="checkbox" required/>
										<label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button>
								</div>
							</div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<div class="mb-xs text-center">
								<a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
								<a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
							</div>

							<p class="text-center">Already have an account? <a href="{{ route('login') }}">Sign In!</a>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2018. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
		<script src="{{ asset('/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
		<script src="{{ asset('/vendor/bootstrap/js/bootstrap.js') }}"></script>
		<script src="{{ asset('/vendor/nanoscroller/nanoscroller.js') }}"></script>
		<script src="{{ asset('/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
		<script src="{{ asset('/vendor/magnific-popup/magnific-popup.js') }}"></script>
		<script src="{{ asset('/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('/js/theme.js') }}"></script>
		
		<!-- Theme Custom -->
		<script src="{{ asset('/js/theme.custom.js') }}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{ asset('/js/theme.init.js') }}"></script>
@endsection