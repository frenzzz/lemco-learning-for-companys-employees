<!doctype html>
<html class="fixed">
    <head>
        @include('components.dashboard-head')
        @yield('title')
        @yield('head')
    </head>
    <body>
        @yield('content')
    </body>
</html>