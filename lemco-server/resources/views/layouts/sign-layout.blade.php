<!doctype html>
<html class="fixed">
    <head>
        @include('components.sign-head')
        @yield('title')
    </head>
    <body>
        @yield('content')
    </body>
</html>