@extends('layouts.dashboard-layout')

@section('title')
<title>LEMCO panel</title>
@endsection

@section('head')
    		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/select2/select2.css')}}" />
        <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />


@endsection

@section('content')

<section class="body">

    @include('components.dashboard-header')
    
    <div class="inner-wrapper">
        
        @include('components.dashboard-navbar')

        <section role="main" class="content-body">
            <header class="page-header">
                <h2>{{ $Course->title }}</h2>
                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="{{ url('dashboard') }}">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('course') }}">
                                <span>Corsi</span>
                            </a>
                        </li>
                        <li><span>{{ $Course->title }}</span></li>
                    </ol>
                </div>
            </header>
            <div class="row">
				<div class="col-lg-12">
                    <section class="panel">
                        <div class="col-lg-6">
                            <form class="form-horizontal form-bordered" method="post" action="{{ url('request/new-lesson') }}?&c={{$Course->id}}&ref={{$_SERVER['REQUEST_URI']}}" enctype="multipart/form-data">
                                @csrf
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Titolo della lezione <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="lesson[title]" class="form-control" placeholder="eg.: John Doe" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Blocco lezioni n°</label>
                                        <div class="col-sm-3">
                                            <input type="number" min="1" name="lesson[id_lessons_block]" class="form-control" placeholder="1234"/>
                                        </div>

                                        <label class="col-sm-3 control-label">Lezione n°</label>
                                        <div class="col-sm-3">
                                            <input type="number" min="1" name="lesson[index_int]" class="form-control" placeholder="1234"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Testo della lezione</label>
                                        <div class="col-sm-9">
                                            <textarea name="lesson[description]" rows="5" class="form-control" placeholder="Descrivi il corso, come è articolato, ecc"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Id del video Youtube</label>
                                        <div class="col-md-9">
                                            <input type="text" name="lesson[path]" class="form-control" placeholder="QdaO4l7a3c4"/>
                                        </div>
                                        <span class="col-lg-12 mt-lg mb-lg line-thru text-center text-uppercase">
                                            <span>or</span>
                                        </span>
                                        <label class="col-sm-3 control-label">Carica il file</label>
                                        <div class="col-md-9">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fa fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">Change</span>
                                                        <span class="fileupload-new">Select file</span>
                                                        <input type="file" name="lesson_file" />
                                                    </span>
                                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button class="btn btn-success">Salva</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>
                                </footer>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <div class="col-lg-10" id="nestable">
                                <ol class="dd-list">
                                    {{!!$temp = null}}
                                    @foreach($Lessons as $key => $les)
                                        @if($key == 0 || $temp->id_lessons_block != $les->id_lessons_block)
                                        <li class="dd-item" data-id="block-{{ $les->id_lessons_block }}">
                                            <div class="dd-handle">Blocco {{ $les->id_lessons_block }}</div>
                                            <ol class="dd-list">
                                        @endif
                                                <li class="dd-item" data-id="lesson-{{ $les->index_int }}"><div class="dd-handle">{{ $les->index_int }} - {{ $les->title }}
                                                    </div>
                                                </li>
                                        @if($key+1 < count($Lessons) && $Lessons[$key+1]->id_lessons_block != $les->id_lessons_block)
                                            </ol>
                                        </li>
                                        @endif
                                        @php
                                            $temp = $les;
                                        @endphp
                                    @endforeach
                                </ol>
                            </div>
                            <div class="col-lg-2">
                                <ol class="dd-list">
                                    {{!!$temp = null}}
                                    @foreach($Lessons as $key => $les)
                                        @if($key == 0 || $temp->id_lessons_block != $les->id_lessons_block)
                                        <li style="visibility: hidden">
                                            <div class="dd-handle"></div>
                                        </li>
                                        @endif
                                                <li class="dd-item" data-id="lesson-{{ $les->index_int }}"><div class="dd-handle">
                                                    <a href="{{ url('request/delete-lesson').'/'.$les->id }}?ref={{$_SERVER['REQUEST_URI']}}" class="on-default remove*row" onclick="return confirm(‘Confermi la cancellazione’);"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </li>
                                        @if($key+1 < count($Lessons) && $Lessons[$key+1]->id_lessons_block != $les->id_lessons_block)

                                        @endif
                                        @php
                                            $temp = $les;
                                        @endphp
                                    @endforeach
                                </ol>
                            </div>
                            <footer class="panel-footer">
                                <textarea id="nestable-output" rows="3" class="form-control"></textarea>
                                <div class="row">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button class="btn btn-primary">Salva posizione</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <hr>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <form class="form-horizontal form-bordered" method="post" action="{{ url('request/new-questionnaire') }}?&c={{$Course->id}}&ref={{$_SERVER['REQUEST_URI']}}" enctype="multipart/form-data">
                                @csrf
                                <div class="panel-body">
                                    <h4>Ogni blocco di lezioni ha un questionario da risolvere</h4>
                                    <h5>Selezione quello che vuoi creare</h5>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Questionario del blocco n°</label>
                                        <div class="col-sm-9">
                                            <select class="form-control mb-md" name="questionnaire[id_lessons_block]">
                                                @foreach($Blocks as $block)
                                                <option value="{{$block->id_lessons_block}}">{{$block->id_lessons_block}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Domanda <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="question[text]" class="form-control" placeholder="eg.: John Doe" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Risposta (CORRETTA) <span class="required">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" name="answer_correct" class="form-control" placeholder="eg.: John Doe" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Risposta (ERRATA)</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="answer_wrong[]" class="form-control" placeholder="eg.: John Doe" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Risposta (ERRATA)</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="answer_wrong[]" class="form-control" placeholder="eg.: John Doe"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Risposta (ERRATA)</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="answer_wrong[]" class="form-control" placeholder="eg.: John Doe"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                                            <thead>
                                                <tr>
                                                    <th>Blocco</th>
                                                    <th>Domanda</th>
                                                    <th>Risposte</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {{!$questionnaires = App\Questionnaire::where('id_course', $Course->id)->get()}}
                                                @foreach($questionnaires as $quest)
                                                    {{!$questionss = $quest->questions}}
                                                    @foreach($questionss as $que)
                                                    <tr class="gradeX">
                                                        <td>{{ $quest->id_lessons_block }}</td>
                                                        <td>{{ $que->text }}</td>
                                                        {{!$answers = $que->answers }}
                                                        <td>
                                                            @foreach($answers as $ans)
                                                            {{ $ans->text }} {{ $ans->is_correct ? '(CORRECT)' : ''}}<br>
                                                            @endforeach
                                                        </td>
                                                        <td class="actions">
                                                            <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                                                            <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                                                            <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                                            <a href="{{ url('request/delete-questionnaire').'/'.$quest->id }}?ref={{$_SERVER['REQUEST_URI']}}" class="on-default remove*row" onclick="return confirm(‘Confermi la cancellazione’);"><i class="fa fa-trash-o"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <footer class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button class="btn btn-success">Salva</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>
                                </footer>
                            </form>
                        </div>                      

                    </section>
                </div>
            </div>
        </section>

    </div>
</section>

<div id="dialog" class="modal-block mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Are you sure?</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                    <p>Are you sure that you want to delete this row?</p>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button id="dialogConfirm" class="btn btn-primary">Confirm</button>
                    <button id="dialogCancel" class="btn btn-default">Cancel</button>
                </div>
            </div>
        </footer>
    </section>
</div>

<!-- Vendor -->
<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

<!-- Specific Page Vendor -->
<script src="{{ asset('/vendor/jquery-nestable/jquery.nestable.js') }}"></script>
<script src="{{ asset('/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>



<!-- Theme Base, Components and Settings -->
<script src="{{ asset('/js/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('/js/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('/js/theme.init.js') }}"></script>


<!-- Examples -->
<script src="{{ asset('/js/ui-elements/examples.nestable.js') }}"></script>


@endsection