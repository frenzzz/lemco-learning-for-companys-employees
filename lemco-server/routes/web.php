<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/register-admin', 'UserController@registerAdmin');

//rotte protette
Route::group(['prefix' => 'api'], function () {
    // Only verified users may enter...ESEMPIO API
        Route::resource('user', 'UserController');
        Route::get('user_with_id/{id}', 'UserController@indexWithId');

        Route::resource('corso', 'CourseController');
        Route::get('corsi_with_id/{id}', 'CourseController@indexWithId');
        Route::get('risposte_corsi_with_id/{id}', 'CourseController@AnswerCourseWithId');

        Route::resource('lezione', 'LessonController');
        Route::get('lezione_with_id/{id}', 'LessonController@indexWithId');
        Route::get('blocco_with_id/{id}', 'LessonController@bloccoWithId');

        Route::resource('questionario', 'QuestionnaireController');
        Route::get('questionario_with_id/{id}', 'QuestionnaireController@indexWithId');

        Route::resource('question', 'QuestionController');
        Route::get('question_with_id/{id}', 'QuestionController@indexWithId');

        Route::resource('risposta', 'AnswerController');
        Route::get('risposta_with_id/{id}', 'AnswerController@indexWithId');
        Route::get('risposta_questionario/{idQuestionnaire}-{idUser}', 'AnswerController@AnswerQuestionnaire');


        Route::resource('file', 'FileController');
        Route::get('file_with_id/{id}', 'FileController@indexWithId');
        
        Route::resource('role', 'RoleController');
        Route::get('role_with_id/{id}', 'RoleController@indexWithId');
        Route::get('user_role/{id_user}-{id_role}', 'RoleController@userRole');
        Route::get('user_role_with_id/{id_store}', 'RoleController@userRoleWithStore');
        Route::get('all_role_user/{id_store}', 'RoleController@allRoleUser');
        Route::get('all_role_course/{id_store}', 'RoleController@allRoleCourse');
});


/*****ROUTE ADMINISTRATO *******/
Route::group(['prefix' => '/dashboard', 'middleware' => 'Admin'], function () {
    Route::get('', function () {
        return view('dashboard');
    });
    Route::get('role', function () {
        return view('role');
    });
    Route::get('course', function () {
        return view('course');
    });
    Route::get('employee', function () {
        return view('employee');
    });
    Route::get('/c-{id}', 'CourseController@singleCourse');

});

Route::group(['prefix' => '/request'], function () {

    /*******ROUTE BUSINESS ROLE *******/
    Route::get('/delete-role/{id}', 'RoleController@deleteRole');
    Route::post('/new-role', 'RoleController@newRole');
    Route::post('/role-course', 'RoleController@roleCourse');
    Route::post('/role-user', 'RoleController@roleUser');

    /*******ROUTE COURSE *******/
    Route::post('/new-course', 'CourseController@newCourse');
    Route::get('/delete-course/{id}', 'CourseController@deleteCourse');

    /*******ROUTE LESSON*******/
    Route::post('/new-lesson', 'LessonController@newLesson');
    Route::get('/delete-lesson/{id}', 'LessonController@deleteLesson');

    /*******ROUTE QUESTIONNAIRE*******/
    Route::post('/new-questionnaire', 'QuestionnaireController@newQuestion');
    Route::get('/delete-questionnaire/{id}', 'QuestionnaireController@deleteQuestion');

    /*******ROUTE USER *******/


    /*******ROUTE ADMINISTRATO *******/
    /*******ROUTE ADMINISTRATO *******/

});