<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'text', 'is_correct', 'id_question',
    ];

    public function userAnswers()
    {
        return $this->hasMany('App\UserAnswer', 'id_answer');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_answer', 'id_answer', 'id_user');
    }
}
