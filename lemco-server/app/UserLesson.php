<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLesson extends Model
{
    protected $fillable = [
        'id_lesson', 'id_user'
    ];

    protected $table = 'user_lesson';

    
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson', 'id_lesson');
    }

}
