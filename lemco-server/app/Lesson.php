<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = ['title', 'description', 'index_int', 'id_lessons_block', 'id_course'];

    public function files()
    {
        return $this->hasMany('App\File', 'id_lesson');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_business_role', 'id_business_role', 'id_user');
    }

    public function userLessons()
    {
        return $this->hasMany('App\UserLesson', 'id_lesson');
    }
}
