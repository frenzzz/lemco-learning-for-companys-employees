<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'text', 'instruction', 'type', 'index', 'id_questionnaire'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer', 'id_question');
    }
}
