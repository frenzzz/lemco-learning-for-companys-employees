<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRole extends Model
{
    protected $fillable = ['name', 'id_store'];

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_business_role', 'id_business_role', 'id_course');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_business_role', 'id_business_role', 'id_user');
    }
}
