<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['title', 'description', 'id_store'];

    public function roles()
    {
        return $this->belongsToMany('App\BusinessRole', 'course_business_role', 'id_course', 'id_business_role');
    }

    public function questionnaires()
    {
        return $this->hasMany('App\Questionnaire', 'id_course');
    }

    public function userAnswers()
    {
        return $this->hasMany('App\UserAnswer', 'id_course');
    }

}
