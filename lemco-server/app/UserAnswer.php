<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $fillable = [
        'id_answer', 'id_user', 'id_question', 'id_course'
    ];

    protected $table = 'user_answer';


    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer', 'id_answer');
    }

    public function question()
    {
        return $this->belongsTo('App\Question', 'id_question');
    }

    public function course()
    {
        return $this->belongsTo('App\Course', 'id_course');
    }
}
