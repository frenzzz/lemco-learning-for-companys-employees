<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'name', 'street', 'city', 'cap', 'code'
    ];

    public function roles()
    {
        return $this->hasMany('App\BusinessRole', 'id_store');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'id_store');
    }
}
