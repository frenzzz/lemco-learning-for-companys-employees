<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request, Response;
use App\File;
use Auth;

class FileController extends Controller
{
    public function indexWithId(Request $request, $id)
    {

       $file = null;

        if ($id != null) {
            //$arrayBlocchi = array();
            $file = File::where('id_lesson', $id)->first();
        }
        return Response::json(
            $file
        );
    }
}
