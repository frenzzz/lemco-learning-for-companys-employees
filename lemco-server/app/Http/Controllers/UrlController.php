<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function index()
    {
        return 'Hello, API';
    }
    
    /** CREATE object corrisponde ad un arichiesta POST
     * $ curl -i --user firstuser:first_password -d 'url=http://google.com&description=A Search Engine' localhost/l4api/public/index.php/api/v1/url
     * HTTP/1.1 201 Created
     * Date: Tue, 21 May 2013 19:10:52 GMT
     * Content-Type: application/json
     * 
     * {"error":false,"message":"URL created"}*/
    public function store()
    {
        $url = new Url;
        $url->url = Request::get('url');
        $url->description = Request::get('description');
        $url->user_id = Auth::user()->id;
    
        $url->save();
    
        return Response::json(array(
            'error' => false,
            'urls' => $urls->toArray()),
            200
        );
    }

    /** RETURN object id = 1
     * $ curl --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * { .. object id=1 .. }*/
    public function show($id)
    {
        $url = Url::where('user_id', Auth::user()->id)
                ->where('id', $id)
                ->take(1)
                ->get();

        return Response::json(array(
            'error' => false,
            'urls' => $url->toArray()),
            200
        );
    }

    /** DELETE
     * $ curl -i -X DELETE --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:24:19 GMT
     * Content-Type: application/json
     * 
     * {"error":false,"message":"url deleted"} */
    public function destroy($id)
    {
        $url = Url::where('user_id', Auth::user()->id)->find($id);

        $url->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'url deleted'),
            200
            );
    }

    /** PUT
     * $ curl -i -X PUT --user seconduser:second_password -d 'url=http://yahoo.com' localhost/l4api/public/index.php/api/v1/url/4
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:34:21 GMT
     * Content-Type: application/json
     * 
     * {"error":false,"message":"url updated"} */
    public function update($id)
    {
        $url = Url::where('user_id', Auth::user()->id)->find($id);

        if ( Request::get('url') )
        {
            $url->url = Request::get('url');
        }

        if ( Request::get('description') )
        {
            $url->description = Request::get('description');
        }

        $url->save();

        return Response::json(array(
            'error' => false,
            'message' => 'url updated'),
            200
        );
    }
}

