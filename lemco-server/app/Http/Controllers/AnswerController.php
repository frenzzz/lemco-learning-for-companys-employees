<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Questionnaire;
use App\UserAnswer;
use Illuminate\Http\Request;
use Response;

class AnswerController extends Controller
{
    public function indexWithId(Request $request, $id)
    {

        if ($id != null) {

            $questions_id = Question::where('id_questionnaire', $id)->orderBy('index', 'ASC')->orderBy('id', 'ASC')->select('id')->get();
            $answers = Answer::whereIn('id_question', $questions_id)->inRandomOrder()->get();

        }
        return Response::json(
            $answers
        );
    }

    public function show($id)
    {
        $ans = Answer::find($id);

        return Response::json(
            $ans
        );
    }

    public function store(Request $request)
    {

        $idUser = $request->id_utente;
        $idQuestion = $request->id_domanda;
        $idAnswer = $request->id_risposta;
        $idQuestionnaire = $request->id_questionario;

        $idCourse = Questionnaire::find($idQuestionnaire)->id_course;
        $ans = UserAnswer::where('id_user', $idUser)->where('id_question', $idQuestion)->count();
        if ($ans <= 0) {
            UserAnswer::create(array(
                'id_user' => $idUser,
                'id_answer' => $idAnswer,
                'id_question' => $idQuestion,
                'id_course' => $idCourse,
            ));
        } else {
            return Response::json(array(
                'error' => true,
                'message' => 'url deleted'),
                402
            );
        }

        return Response::json(array(
            'error' => false,
        ),
            200
        );
    }

    /**********************************end Api *****************************/

    public function AnswerQuestionnaire(Request $request, $idQuestionnaire, $idUser) {
        $questionnaire = Questionnaire::find($idQuestionnaire);
        $questions = $questionnaire->questions;
        $domandeNonRisposte = array('completo'=>true, 'domande' => array());
        foreach($questions as $question){
            $temp = UserAnswer::where('id_user', $idUser)->where('id_question', $question->id)->count();
            if($temp <= 0){
                array_push($domandeNonRisposte['domande'], $question);
                $domandeNonRisposte['completo'] = false;
            }
        }

        return $domandeNonRisposte;
    }
}
