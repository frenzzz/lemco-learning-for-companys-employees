<?php

namespace App\Http\Controllers;

use App\BusinessRole;
use App\Course;
use App\Lesson;
use App\User;
use App\UserAnswer;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;

class CourseController extends Controller
{

    /** CREATE object corrisponde ad un arichiesta POST
     * $ curl -i --user firstuser:first_password -d 'url=http://google.com&description=A Search Engine' localhost/l4api/public/index.php/api/v1/url
     * HTTP/1.1 201 Created
     * Date: Tue, 21 May 2013 19:10:52 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"URL created"}*/
    public function store(Request $request)
    {

        $idRuolo = $request->id_ruolo;
        $arrayCorsi = $request->id_corsi;

        $delete = DB::table('course_business_role')->where('id_business_role', $idRuolo)->whereNotIn('id_course', $arrayCorsi)->get();
        DB::table('course_business_role')->where('id_business_role', $idRuolo)->delete();
        $ruolo = BusinessRole::find($idRuolo);

        foreach ($arrayCorsi as $idcorso) {

            $corso = null;
            $corso = Course::find($idcorso);
            $ruolo->courses()->attach($corso);
        }

        return Response::json(
            $delete
        );
    }

    /** RETURN object id = 1
     * $ curl --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * { .. object id=1 .. }*/
    public function show($id)
    {
        $course = Course::find($id);

        return Response::json(
            $course
        );
    }

    /** DELETE
     * $ curl -i -X DELETE --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:24:19 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url deleted"} */
    public function destroy($id)
    {
        $role = BusinessRole::findOrFail($id);
        $role->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'role deleted'),
            200
        );
    }

    /** PUT
     * $ curl -i -X PUT --user seconduser:second_password -d 'url=http://yahoo.com' localhost/l4api/public/index.php/api/v1/url/4
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:34:21 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url updated"} */
    public function update($id)
    {
        $role = BusinessRole::findOrFail($id);

        if (Request::get('role_name')) {
            $role->name = Request::get('role_name');
        }

        $role->save();

        return Response::json(array(
            'error' => false,
            'message' => 'role updated'),
            200
        );
    }

    /**********************************end Api *****************************/

    public function newCourse(Request $request)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $course = Course::create(array(
                'title' => $request->course['title'],
                'description' => $request->course['description'],
                'id_store' => Auth::user()->id_store,
            ));

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function deleteCourse(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $course = Course::findOrFail($id);
            $course->delete();

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function singleCourse(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $course = Course::findOrFail($id);
            $lessons = Lesson::where('id_course', $course->id)->orderBy('id_lessons_block', 'ASC')->orderBy('index_int', 'ASC')->get();
            $blocks = Lesson::where('id_course', $course->id)->orderBy('id_lessons_block', 'ASC')->select('id_lessons_block')->distinct()->get();

            if ($course != null) {
                return view('detailCourse', ['Course' => $course, 'Lessons' => $lessons, 'Blocks' => $blocks]);
            } else {
                return abort(404, 'Pagina non trovata.');
            }

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function indexWithId(Request $request, $id)
    {
        $courses = null;
        $user = User::find($id);

        if ($user != null) {

            if ($user->is_administrator == '1') {

                $courses = Course::where('id_store', $user->id_store)->get();

            } else {
                $arrayRo = array();
                $roles = DB::table('user_business_role')->where('id_user', $user->id)->select('id_business_role')->distinct()->get();
                foreach ($roles as $key => $ro) {
                    $arrayRo[$key] = $ro->id_business_role;
                }

                $courses_id = DB::table('course_business_role')->whereIn('id_business_role', $arrayRo)->select('id_course')->distinct()->get();
                foreach ($courses_id as $key => $idCor) {
                    $arrayRo[$key] = $idCor->id_course;
                }

                $courses = Course::whereIn('id', $arrayRo)->get();
            }

        } else {
            return 'Error';
        }

        return Response::json(
            $courses
        );
    }

    public function AnswerCourseWithId(Request $request, $id)
    {
        $courses = null;
        $user = User::find($id);

        if ($user != null) {

            if ($user->is_administrator == '1') {

                $arrayRo = Course::where('id_store', $user->id_store)->select('id')->orderBy('id', 'ASC')->distinct()->get();

            } else {

                $arrayRo = array();
                $roles = DB::table('user_business_role')->where('id_user', $user->id)->select('id_business_role')->distinct()->get();
                foreach ($roles as $key => $ro) {
                    $arrayRo[$key] = $ro->id_business_role;
                }

                $courses_id = DB::table('course_business_role')->whereIn('id_business_role', $arrayRo)->select('id_course')->distinct()->get();
                foreach ($courses_id as $key => $idCor) {
                    $arrayRo[$key] = $idCor->id_course;
                }
            }

            $arrayAnswers = UserAnswer::whereIn('id_course', $arrayRo)->get();
            $total = count($arrayAnswers);
            $arr = array('TotAnswer' => $total, 'TotCorrect' => 0, 'courseList' => array());

            foreach ($arrayAnswers as $ansUs) {

                if (array_key_exists($ansUs->id_course, $arr['courseList'])) {
                    $corrBol = false;
                    if ($ansUs->answer->is_correct == 1) {
                        $arr['TotCorrect']++;
                        $arr['courseList'][$ansUs->id_course]['correct']++;
                    }
                    $arr['courseList'][$ansUs->id_course]['answer']++;
                } else {
                    $corrBol = false;
                    if ($ansUs->answer->is_correct == 1) {
                        $arr['TotCorrect']++;
                        $corrBol = true;
                    }
                    $arr['courseList'][$ansUs->id_course] = array('course' => $ansUs->course, 'answer' => 1, 'correct' => ($corrBol ? 1 : 0));
                }
            }

        } else {
            return 'Error';
        }

        return Response::json(
            $arr
        );
    }

}
