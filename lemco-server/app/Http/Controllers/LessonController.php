<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Controllers\Tool;
use App\Lesson;
use App\UserLesson;
use Auth;
use Illuminate\Http\Request;
use Response;

class LessonController extends Controller
{
    public function indexWithId(Request $request, $id)
    {

        //$arrayBlocchi = null;
        if ($id != null) {
            //$arrayBlocchi = array();
            $lessons = Lesson::where('id_course', $id)->orderBy('id_lessons_block', 'ASC')->orderBy('index_int', 'ASC')->get();

/*
foreach ($lessons as $lesson) {
$id_block = $lesson->id_lessons_block;

if (array_key_exists($id_block, $arrayBlocchi)) {
$arr = null;
$arr = $arrayBlocchi[$id_block]['lezioni'];
array_push($arr, $lesson);
$arrayBlocchi[$id_block] = ['lezioni' => $arr];
} else {
$arr = array();
array_push($arr, $lesson);
$arrayBlocchi[$id_block] = ['lezioni' => $arr];
}
}
 */
        }
        return Response::json(
            //$arrayBlocchi
            $lessons
        );
    }

    public function bloccoWithId(Request $request, $id)
    {
        $blocchi = null;
        if ($id != null) {
            $blocchi = Lesson::where('id_course', $id)->select('id_lessons_block')->distinct()->orderBy('id_lessons_block', 'ASC')->get();
        }
        return Response::json(
            $blocchi
        );
    }
    /* ***********funzione per creare json con tutta la lista di lezioni raggruppata per id block**********
    public function bloccoWithId(Request $request, $id)
    {
    $arrayBlocchi = null;
    if ($id != null) {
    $arrayBlocchi = array();
    $lessons = Lesson::where('id_course', $id)->orderBy('id_lessons_block', 'ASC')->orderBy('index_int', 'ASC')->get();

    foreach ($lessons as $lesson) {
    $id_block = $lesson->id_lessons_block;

    if (array_key_exists($id_block, $arrayBlocchi)) {
    $arr = null;
    $arr = $arrayBlocchi[$id_block]['lezioni'];
    array_push($arr, $lesson);
    $arrayBlocchi[$id_block] = ['lezioni' => $arr];
    } else {
    $arr = array();
    array_push($arr, $lesson);
    $arrayBlocchi[$id_block] = ['lezioni' => $arr];
    }
    }

    }

    return Response::json(
    $arrayBlocchi
    );
    }
     */

    public function show($id)
    {
        $str_arr = explode("-", $id);
        $idLesson = $str_arr[0];
        $lesson = Lesson::find($idLesson);
        $idUser = $str_arr[1];

        $cont = UserLesson::where('id_user', $idUser)->where('id_lesson', $idLesson)->count();
        if ($cont <= 0) {
            UserLesson::create(array(
                'id_lesson' => $idLesson,
                'id_user' => $idUser,
            ));
        }
        
        return Response::json(
            $lesson
        );
    }

    /**********************************end Api *****************************/

    public function newLesson(Request $request)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $lesson = Lesson::create(array(
                'title' => $request->lesson['title'],
                'description' => $request->lesson['description'],
                'index_int' => $request->lesson['index_int'],
                'id_lessons_block' => $request->lesson['id_lessons_block'],
                'id_course' => $request->c,
            ));

            if ($request->lesson['path'] != null && $request->lesson['path'] != '') {
                $file = File::create(array(
                    'name' => $request->lesson['title'],
                    'path' => $request->lesson['path'],
                    'type' => 'STREAMING',
                    'id_lesson' => $lesson->id,
                ));
            } else {
                $name = Tool::normalize($request->lesson_file->getClientOriginalName());
                $request->file('lesson_file')->storeAs('/file', $name, 'public');
                $file = File::create(array(
                    'name' => $name,
                    'path' => "/file/$name",
                    'type' => 'DOWNLOAD',
                    'id_lesson' => $lesson->id,
                ));
            }

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function deleteLesson(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $lesson = Lesson::findOrFail($id);

            $files = $lesson->files;
            foreach ($files as $file) {
                $path = $file->path;
                if ($path != "" && file_exists('storage' . $path)) {
                    unlink(public_path('storage' . $path));
                }
            }

            $lesson->delete();

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

}
