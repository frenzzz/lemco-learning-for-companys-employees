<?php

namespace App\Http\Controllers;

use App\Store;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Response;

class UserController extends Controller
{
    public function index()
    {
        return 'Hello, API';
    }

    /** CREATE object corrisponde ad un arichiesta POST
     * $ curl -i --user firstuser:first_password -d 'url=http://google.com&description=A Search Engine' localhost/l4api/public/index.php/api/v1/url
     * HTTP/1.1 201 Created
     * Date: Tue, 21 May 2013 19:10:52 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"URL created"}*/
    public function store(Request $request)
    {

        $user = User::where('email', $request->user['email'])->get();

        $user->name_surname = $request->user['name_surname'];
        $user->email = $request->user['email'];
        $user->phone = $request->user['phone'];
        if ($request->user['is_administrator']) {
            $user->is_administrator = true;
        }
        $user->password = Hash::make($request->user['password']);
        $user->name_surname = $request->user['name_surname'];

        $user->user_id = Auth::user()->id;

        $url->save();

        return Response::json(array(
            'error' => false,
            'urls' => $urls->toArray()),
            200
        );
    }

    /** RETURN object id = 1
     * $ curl --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * { .. object id=1 .. }*/
    public function show($id)
    {
        $url = Url::where('user_id', Auth::user()->id)
            ->where('id', $id)
            ->take(1)
            ->get();

        return Response::json(array(
            'error' => false,
            'urls' => $url->toArray()),
            200
        );
    }

    /** DELETE
     * $ curl -i -X DELETE --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:24:19 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url deleted"} */
    public function destroy($id)
    {
        $url = Url::where('user_id', Auth::user()->id)->find($id);

        $url->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'url deleted'),
            200
        );
    }

    /** PUT
     * $ curl -i -X PUT --user seconduser:second_password -d 'url=http://yahoo.com' localhost/l4api/public/index.php/api/v1/url/4
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:34:21 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url updated"} */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->name_surname !== $request->nUtente['name_surname']) {
            $user->update(['name_surname' => $request->nUtente['name_surname']]);
        }

        if ($user->email !== $request->nUtente['email']) {
            $user->update(['email' => $request->nUtente['email']]);
        }

        if ($user->phone !== $request->nUtente['phone']) {
            $user->update(['phone' => $request->nUtente['phone']]);
        }

        if ($request->nUtente['password'] != null && $request->nUtente['password'] != '') {
            $pas = Hash::make($request->nUtente['password']);
            if ($user->password !== $pas) {
                $user->update(['password' => $pas]);
            }
        }

        if($user->is_administrator == 1){
            $store = $user->store;

            if ($store->name !== $request->nUtente['name']) {
                $store->update(['name' => $request->nUtente['name']]);
            }
    
            if ($store->street !== $request->nUtente['street']) {
                $store->update(['street' => $request->nUtente['street']]);
            }
    
            if ($store->cap !== $request->nUtente['cap']) {
                $store->update(['cap' => $request->nUtente['cap']]);
            }

            if ($store->code !== $request->nUtente['code']) {
                $store->update(['code' => $request->nUtente['code']]);
            }
        }



        return Response::json(array(
            'error' => false,
            'utente' => $user,
            'message' => 'Informazioni aggiornate'),
            200
        );
    }

    /**********************************end Api *****************************/


    public function registerAdmin(Request $request)
    {
        $validatedData = $request->validate([
            'name_surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'min:8', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'password_confirm' => ['required', 'string', 'min:8', 'same:password'],

            'company_name' => ['required', 'string', 'max:255'],
            'company_street' => ['required', 'string', 'max:255'],
            'company_city' => ['required', 'string', 'max:255'],
            'company_cap' => ['required', 'string', 'max:255'],
        ]);

        if (count(User::where('email', $request->email)->get()) == 0) {
            if ($request->password == $request->password_confirm) {
                $store = new Store;

                $store->name = $request->company_name;
                $store->street = $request->company_street;
                $store->city = $request->company_city;
                $store->cap = $request->company_cap;
                $store->code = Hash::make($request->company_name . $request->email);
                $store->save();

                $id_store = $store->id;

                User::create(array(
                    'name_surname' => $request->name_surname,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => Hash::make($request->password),
                    'is_administrator' => true,
                    'id_store' => $id_store,
                ));

                return redirect('sign-in');

            } else {
                return Response::json(array(
                    'error' => true,
                    'message' => 'Le email non coincidono'),
                    404
                );
            }

        } else {
            return Response::json(array(
                'error' => true,
                'message' => 'Questa email è già presente nel database'),
                404
            );
        }

    }

    public function indexWithId(Request $request, $id)
    {

        if ($id != null) {
            $users= User::where('id_store', $id)->where('is_administrator', '0')->orderBy('name_surname', 'ASC')->get();
        }
        return Response::json(
            $users
        );
    }

}
