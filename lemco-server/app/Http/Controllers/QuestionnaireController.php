<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Questionnaire;
use Auth;
use Illuminate\Http\Request;
use Response;

class QuestionnaireController extends Controller
{

    public function indexWithId(Request $request, $id)
    {

        // $arrayBlocchi = null;

        if ($id != null) {
            //$arrayBlocchi = array();
            $questionnaires = Questionnaire::where('id_course', $id)->orderBy('id_lessons_block', 'ASC')->get();
            /*
        foreach ($questionnaires as $quest) {
        $id_block = $quest->id_lessons_block;

        if (array_key_exists($id_block, $arrayBlocchi)) {
        $arr = null;
        $arr = $arrayBlocchi[$id_block];
        array_push($arr, $quest);
        $arrayBlocchi[$id_block] = $arr;
        } else {
        $arr = array();
        array_push($arr, $quest);
        $arrayBlocchi[$id_block] = $arr;
        }
        }
         */
        }
        return Response::json(
            $questionnaires
        );
    }

    public function show($id)
    {
        $questionnaire = Questionnaire::find($id);

        return Response::json(
            $questionnaire
        );
    }

    /**********************************end Api *****************************/

    public function newQuestion(Request $request)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $questionnaire = Questionnaire::where('id_course', $request->c)->where('id_lessons_block', $request->questionnaire['id_lessons_block'])->first();

            if ($questionnaire == null) {
                $questionnaire = Questionnaire::create(array(
                    //'title' => $request->questionnaire['title'],
                    'title' => $request->questionnaire['id_lessons_block'] . ' - Questionario',
                    //'description' => $request->questionnaire['description'],
                    'id_lessons_block' => $request->questionnaire['id_lessons_block'],
                    'id_course' => $request->c,
                ));
            }

            $question = Question::create(array(
                'text' => $request->question['text'],
                'id_questionnaire' => $questionnaire->id,
            ));

            $answer_1 = Answer::create(array(
                'text' => $request->answer_correct,
                'is_correct' => true,
                'id_question' => $question->id,
            ));

            for ($i = 0; $i < count($request->answer_wrong); $i++) {
                if ($request->answer_wrong[$i] != null) {
                    $answer_2 = Answer::create(array(
                        'text' => $request->answer_wrong[$i],
                        'is_correct' => false,
                        'id_question' => $question->id,
                    ));
                }
            }

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function deleteQuestion(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $Question = Questionnaire::findOrFail($id);
            $Question->delete();

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }
}
