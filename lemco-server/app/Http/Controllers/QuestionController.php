<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request, Response;
use Auth;
use App\Question;

class QuestionController extends Controller
{
    
    public function indexWithId(Request $request, $id)
    {

       // $arrayBlocchi = null;

        if ($id != null) {
            //$arrayBlocchi = array();
            $questions = Question::where('id_questionnaire', $id)->orderBy('index', 'ASC')->orderBy('id', 'ASC')->get();
       /* 
            foreach ($questionnaires as $quest) {
                $id_block = $quest->id_lessons_block;

                if (array_key_exists($id_block, $arrayBlocchi)) {
                    $arr = null;
                    $arr = $arrayBlocchi[$id_block];
                    array_push($arr, $quest);
                    $arrayBlocchi[$id_block] = $arr;
                } else {
                    $arr = array();
                    array_push($arr, $quest);
                    $arrayBlocchi[$id_block] = $arr;
                }
            }
            */
        }
        return Response::json(
            $questions
        );
    }
 }
