<?php

namespace App\Http\Controllers;

use App\BusinessRole;
use App\Course;
use App\Store;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;

class RoleController extends Controller
{

    public function index()
    {
        $roles = BusinessRole::all();

        return Response::json(array(
            'error' => false,
            'roles' => $roles->toArray()),
            200
        );
    }

    /** CREATE object corrisponde ad un arichiesta POST
     * $ curl -i --user firstuser:first_password -d 'url=http://google.com&description=A Search Engine' localhost/l4api/public/index.php/api/v1/url
     * HTTP/1.1 201 Created
     * Date: Tue, 21 May 2013 19:10:52 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"URL created"}*/
    public function store(Request $request)
    {

        $idRuolo = $request->id_ruolo;
        $arrayUtente = $request->id_utenti;

        $delete = DB::table('user_business_role')->where('id_business_role', $idRuolo)->whereNotIn('id_user', $arrayUtente)->get();
        DB::table('user_business_role')->where('id_business_role', $idRuolo)->delete();
        $ruolo = BusinessRole::find($idRuolo);

        foreach ($arrayUtente as $idUtente) {

            $utente = null;
            $utente = User::find($idUtente);
            $ruolo->users()->attach($utente);
        }

        return Response::json(
            $delete
        );
    }

    /** RETURN object id = 1
     * $ curl --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * { .. object id=1 .. }*/
    public function show($id)
    {
        $role = BusinessRole::find($id);

        return Response::json(array(
            'error' => false,
            'roles' => $role->toArray()),
            200
        );
    }

    /** DELETE
     * $ curl -i -X DELETE --user firstuser:first_password localhost/l4api/public/index.php/api/v1/url/1
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:24:19 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url deleted"} */
    public function destroy($id)
    {
        $role = BusinessRole::findOrFail($id);
        $role->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'role deleted'),
            200
        );
    }

    /** PUT
     * $ curl -i -X PUT --user seconduser:second_password -d 'url=http://yahoo.com' localhost/l4api/public/index.php/api/v1/url/4
     * HTTP/1.1 200 OK
     * Date: Tue, 21 May 2013 19:34:21 GMT
     * Content-Type: application/json
     *
     * {"error":false,"message":"url updated"} */
    public function update($id)
    {
        $role = BusinessRole::findOrFail($id);

        if (Request::get('role_name')) {
            $role->name = Request::get('role_name');
        }

        $role->save();

        return Response::json(array(
            'error' => false,
            'message' => 'role updated'),
            200
        );
    }

    /**********************************end Api *****************************/

    public function indexWithId(Request $request, $id)
    {

        $store = Store::find($id);

        if ($id != null) {
            //$arrayBlocchi = array();
            $roles = $store->roles;

        }
        return Response::json(
            $roles
        );
    }

    public function newRole(Request $request)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $role = BusinessRole::create(array(
                'name' => $request->role_name,
                'id_store' => Auth::user()->id_store,
            ));

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }
    }

    public function deleteRole(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->is_administrator) {
            $role = BusinessRole::findOrFail($id);
            $role->delete();

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }

    }

    public function roleCourse(Request $request)
    {

        if (Auth::check() && Auth::user()->is_administrator) {
            if ($request->role != null && count($request->role) > 0) {
                foreach ($request->role as $id_role => $array_role) {
                    $ro = BusinessRole::findOrFail($id_role);

                    if ($array_role != null && count($array_role) > 0) {
                        foreach ($array_role as $id_course) {
                            $cat = Course::findOrFail($id_course);
                            $ro->courses()->attach($cat);
                        }
                    }
                }
            }

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }
    }

    public function roleUser(Request $request)
    {

        if (Auth::check() && Auth::user()->is_administrator) {
            if ($request->role != null && count($request->role) > 0) {
                foreach ($request->role as $id_role => $array_role) {
                    $ro = BusinessRole::findOrFail($id_role);

                    if ($array_role != null && count($array_role) > 0) {
                        foreach ($array_role as $id_user) {
                            $us = User::findOrFail($id_user);
                            $ro->users()->attach($us);
                        }
                    }
                }
            }

            $path = $request->ref;
            $path = substr($path, 1, strlen($path));
            return redirect($path . '?openAlert=Dati%20inviati%20con%20successo!');

        } else {
            return abort(401, 'Azione non autorizzata!');
        }
    }

    public function userRole(Request $request, $id_user, $id_role)
    {
        $user = User::find($id_user);
        $ro = $user->roles->where('id', $id_role);

        return $ro;
    }

    public function userRoleWithStore(Request $request, $id_store)
    {

        $roleArray = BusinessRole::where('id_store', $id_store)->select('id')->distinct()->get();
        $ro = DB::table('user_business_role')->whereIn('id_business_role', $roleArray)->
            rightJoin('users', 'user_business_role.id_user', '=', 'users.id')->where('is_administrator', '0')->orderBy('id_business_role', 'ASC')->get();

        return $ro;
    }

    public function allRoleUser(Request $request, $id_store)
    {
        $roleArray = BusinessRole::where('id_store', $id_store)->select('id')->distinct()->get();
        $tableRole = DB::table('user_business_role')->whereIn('id_business_role', $roleArray)->get();

        $arrOut = array();
        foreach ($tableRole as $tab) {

            $idUser = $tab->id_user;
            if (array_key_exists($idUser, $arrOut)) {
                $arr = null;
                $arr = $arrOut[$idUser];
                array_push($arr, $tab->id_business_role);
                $arrOut[$idUser] = $arr;

            } else {
                $arr = array();
                array_push($arr, $tab->id_business_role);
                $arrOut[$idUser] = $arr;
            }
        }

        return Response::json(
            $arrOut
        );
    }

    public function allRoleCourse(Request $request, $id_store)
    {
        $roleArray = BusinessRole::where('id_store', $id_store)->select('id')->distinct()->get();
        $tableRole = DB::table('course_business_role')->whereIn('id_business_role', $roleArray)->get();

        $arrOut = array();
        foreach ($tableRole as $tab) {

            $idCourse = $tab->id_course;
            if (array_key_exists($idCourse, $arrOut)) {
                $arr = null;
                $arr = $arrOut[$idCourse];
                array_push($arr, $tab->id_business_role);
                $arrOut[$idCourse] = $arr;

            } else {
                $arr = array();
                array_push($arr, $tab->id_business_role);
                $arrOut[$idCourse] = $arr;
            }
        }

        return Response::json(
            $arrOut
        );
    }
}
