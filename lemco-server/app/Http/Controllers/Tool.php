<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Tool extends Controller
{
    public static function normalize($input){
        $input = str_replace('(', '', $input);
        $input = str_replace(')', '', $input);
        $input = str_replace(' ', '_', $input);
        $input = str_replace('\\', '_', $input);
        $input = str_replace('/', '_', $input);
        $input = str_replace('+', '_', $input);
        $input = str_replace('*', '', $input);
        $input = str_replace('%', '', $input);
        $input = str_replace('#', '', $input);
        $input = str_replace('@', '', $input);
        $input = str_replace('$', '', $input);
        $input = str_replace('{', '', $input);
        $input = str_replace('}', '', $input);
        $input = str_replace('[', '', $input);
        $input = str_replace(']', '', $input);
        $input = str_replace('|', '', $input);
        $input = strtolower($input);
        $input = rand(1 , 99999).$input;
        return $input;
    }

}
