<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request, Response;
use Illuminate\Support\Facades\Auth;
use Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            //'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            //'access_token' => $tokenResult->accessToken,
            'X-XSRF-TOKEN' => $token->id,
            'token_type' => 'Bearer',
            'utente' => $user,
            'store' => $user->store,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ])->header('X-XSRF-TOKEN', $token->id);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name_surname' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => ['required', 'string', 'min:8'],
            'password_confirm' => ['required', 'string', 'min:8', 'same:password'],
        ]);

        $store = Store::where('code', $request->code)->first();

        if ($store != null) {
            $user = User::create(array(
                'name_surname' => $request->name_surname,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
                'is_administrator' => false,
                'id_store' => $store->id,
            ));

            return response()->json([
                'message' => 'Successfully created user!',
                'utente' => $user,
                'store' => $store,
            ], 201);
        } else {
            return "ERRORE NESSUNO STORE TROVATO";
        }

    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
