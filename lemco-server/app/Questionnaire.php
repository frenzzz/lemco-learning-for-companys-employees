<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'title', 'description', 'id_lessons_block', 'id_course'
    ];

    public function questions()
    {
        return $this->hasMany('App\Question', 'id_questionnaire');
    }
}
