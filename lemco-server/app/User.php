<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_surname', 'email', 'password', 'phone', 'id_store', 'is_administrator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\BusinessRole', 'user_business_role', 'id_user', 'id_business_role');
    }

    public function store()
    {
        return $this->belongsTo('App\Store', 'id_store');
    }

    public function answers()
    {
        return $this->belongsToMany('App\Answer', 'user_answer', 'id_user', 'id_answer');
    }

    public function userAnswers()
    {
        return $this->hasMany('App\UserAnswer', 'id_user');
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Lesson', 'user_lesson', 'id_user', 'id_lesson');
    }

    public function userLessons()
    {
        return $this->hasMany('App\UserLesson', 'id_user');
    }
}
