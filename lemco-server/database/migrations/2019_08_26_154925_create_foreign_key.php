<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->foreign('id_question')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::table('business_roles', function (Blueprint $table) {
            $table->foreign('id_store')->references('id')->on('stores')->onDelete('cascade');
        });

        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('id_store')->references('id')->on('stores')->onDelete('cascade');
        });

        Schema::table('course_business_role', function (Blueprint $table) {
            $table->foreign('id_course')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('id_business_role')->references('id')->on('business_roles')->onDelete('cascade');
        });

        Schema::table('lessons', function (Blueprint $table) {
            $table->foreign('id_course')->references('id')->on('courses')->onDelete('cascade');
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->foreign('id_questionnaire')->references('id')->on('questionnaires')->onDelete('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_store')->references('id')->on('stores')->onDelete('cascade');
        });

        Schema::table('user_answer', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_answer')->references('id')->on('answers')->onDelete('cascade');
            $table->foreign('id_question')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::table('user_business_role', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_business_role')->references('id')->on('business_roles')->onDelete('cascade');
        });

        Schema::table('user_lesson', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_lesson')->references('id')->on('lessons')->onDelete('cascade');
        });

        Schema::table('questionnaires', function (Blueprint $table) {
            $table->foreign('id_course')->references('id')->on('courses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_answer', function (Blueprint $table) {
            //
        });
    }
}
