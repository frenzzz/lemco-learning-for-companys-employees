import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { EmployeeGuard } from './guard/employee.guard';
import { AdminGuard } from './guard/admin.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs-employee',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },

  {
    path: 'tabs-employee', loadChildren: './tabs/tabs-employee/tabs-employee.module#TabsEmployeePageModule',
    canActivateChild: [EmployeeGuard]
  },
  {
    path: 'tabs-admin', loadChildren: './tabs/tabs-admin/tabs-admin.module#TabsAdminPageModule',
    canActivateChild: [AdminGuard]
  },
  
  { path: 'lezione', loadChildren: './pages/lezione/lezione.module#LezionePageModule' },
  { path: 'questionario', loadChildren: './pages/questionario/questionario.module#QuestionarioPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
