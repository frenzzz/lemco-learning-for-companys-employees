import {Injectable} from '@angular/core';
import {CanActivate, CanActivateChild} from '@angular/router';
import {UtenteService} from '../services/utente.service';
import {NavController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import { Utente } from '../model/utente.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeGuard implements CanActivate, CanActivateChild {
  constructor(private utenteService: UtenteService, private navController: NavController) {
  }

  canActivate(): Observable<boolean> {
      return this.utenteService.getUtente()
          .pipe(
              take(1),
              map((utente: Utente) => {
                  if (utente == null || utente.is_administrator != 0) {
                      this.navController.navigateRoot('login');
                      return false;
                  }
                  return true;
              })
          );
  }

  canActivateChild(): Observable<boolean> {
      return this.canActivate();
  }

}