import { Component, OnInit } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtenteService } from './services/utente.service';
import { Utente } from './model/utente.model';
import { BehaviorSubject } from 'rxjs';
import { take, map } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  private utente$: BehaviorSubject<Utente>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private utenteService: UtenteService,
    private navController: NavController,

  ) {
    this.initializeApp();
  }


  ngOnInit(): void {
    this.utente$ = this.utenteService.getUtente();
    
    this.utente$.pipe(take(1), map((utente: Utente) => {
      if (utente != null) {
        if (utente.is_administrator === 1) {
          this.navController.navigateRoot('tabs-admin');
        } else {
          this.navController.navigateRoot('tabs-employee');
        }
      } else this.navController.navigateRoot('login');

    }));


  }

  logout() {
    this.utenteService.logout();
    this.navController.navigateRoot('login');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
