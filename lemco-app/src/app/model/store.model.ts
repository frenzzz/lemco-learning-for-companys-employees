export class Store {

    id: BigInteger;
    name: string;
    street: string;
    city: string;
    cap: string;
    code: string;

}
