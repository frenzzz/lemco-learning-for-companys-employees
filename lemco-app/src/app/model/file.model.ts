export class File {

    id: BigInteger;
    name: string;
    path: string;
    type: string;
    id_lesson: BigInteger;
    created_at: string;
    updated_at: string;

}
