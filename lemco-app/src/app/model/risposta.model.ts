import { Corso } from './corso.model';
import { Domanda } from './domanda.model';

export class Risposta {

    id: BigInteger;
    text: string;
    is_correct: boolean;
    id_question: BigInteger;
    created_at: string;
    updated_at: string;

}

export class RisposteStatistiche {
    TotAnswer: number;
    TotCorrect: number;
    courseList: CorsiStatistica[];
}

export class CorsiStatistica {
    answer: number;
    correct: number;
    course : Corso;
}

export class RisposteQuestionario {
    completo: boolean;
    domande: Domanda[];
}