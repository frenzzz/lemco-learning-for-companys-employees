import { Corso } from './corso.model';

export class Questionario {

    id: BigInteger;
    title: string;
    description: string;
    id_lessons_block: number;
    id_course: BigInteger;
    corso: Corso;

}