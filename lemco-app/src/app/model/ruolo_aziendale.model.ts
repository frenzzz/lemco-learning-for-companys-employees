import { Store } from './store.model';
import { Utente } from './utente.model';

export class RuoloAziendale {

    id: BigInteger;
    name: string;
    id_store: BigInteger;
    store: Store;
    utenti: Utente[];
}
