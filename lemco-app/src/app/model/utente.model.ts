import { Store } from './store.model';

export class Utente {

    id: BigInteger;
    name_surname: string;
    email: string;
    phone: string;
    is_administrator: number;
    email_verified_at: string;
    id_store: BigInteger;

}

export class UtenteRuolo extends Utente{
    id_business_role: BigInteger;
}