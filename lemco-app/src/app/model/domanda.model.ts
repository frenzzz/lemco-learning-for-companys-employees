export class Domanda {

    id: BigInteger;
    text: string;
    instruction: string;
    type: string;
    index: number;
    id_questionnaire: BigInteger;
    created_at: string;
    updated_at: string;

}
