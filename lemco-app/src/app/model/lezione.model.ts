import { Corso } from './corso.model';

export class Lezione {

    id: BigInteger;
    title: string;
    description: string;
    index_int: number;
    id_lessons_block: number;
    corso: Corso;

}
