export const USE_EMULATOR = false;

export const URL_BASE = (USE_EMULATOR ? 'http://10.0.2.2' : 'http://localhost') + ':8000/api';
//export const URL_BASE = (USE_EMULATOR ? 'http://10.0.2.2' : 'http://192.168.1.11') + '/api';

export const URL = {
    LOGIN: URL_BASE + '/auth/login',
    LOGOUT: URL_BASE + '/auth/logout',
    UTENTE: URL_BASE + '/auth/user',
    REGISTER: URL_BASE + '/auth/register',

    UTENTE_WITH_ID: URL_BASE + '/user_with_id',
    UPDATE_PROFILO: URL_BASE + '/user',

    CORSO: URL_BASE + '/corso',
    CORSI_WITH_ID: URL_BASE + '/corsi_with_id',
    RISPOSTE_CORSI: URL_BASE + '/risposte_corsi_with_id',

    LEZIONE: URL_BASE + '/lezione',
    LEZIONE_WITH_ID: URL_BASE + '/lezione_with_id',

    QUESTIONARIO: URL_BASE + '/questionario',
    QUESTIONARIO_WITH_ID: URL_BASE + '/questionario_with_id',

    DOMANDA: URL_BASE + '/question',
    DOMANDA_WITH_ID: URL_BASE + '/question_with_id',

    RISPOSTA: URL_BASE + '/risposta',
    RISPOSTA_WITH_ID: URL_BASE + '/risposta_with_id',
    RISPOSTE_QUESTIONARIO: URL_BASE + '/risposta_questionario',

    BLOCCO_WITH_ID: URL_BASE + '/blocco_with_id',

    FILE: URL_BASE + '/file',
    FILE_WITH_ID: URL_BASE + '/file_with_id',

    RUOLO: URL_BASE + '/role',
    RUOLO_WITH_ID: URL_BASE + '/role_with_id',
    TUTTI_RUOLI_UTENTE: URL_BASE + '/all_role_user',
    TUTTI_RUOLI_CORSO: URL_BASE + '/all_role_course',

    UTENTE_RUOLO: URL_BASE + '/user_role',
    UTENTE_RUOLO_WITH_STORE: URL_BASE + '/user_role_with_id',
    
};

export const X_AUTH = 'X-XSRF-TOKEN';

export const AUTH_TOKEN = 'auth-token';

export const UTENTE_STORAGE = 'utente';
export const CORSI_STORAGE = 'corsi';
export const STORE_STORAGE = 'store';
export const IMAGE_PROFILE = 'image-profile';

export const LINGUA = 'lingua';

export const MESSAGE_RESPONSE = 'message';
export const MESSAGGIO_INVITO = 'Utilizza il seguente codice per registrarti su LEMCO. Buon divertimento! Codice: ';
