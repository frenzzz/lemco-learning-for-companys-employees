import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController, ModalController } from '@ionic/angular';
import { UtenteService, Account } from 'src/app/services/utente.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utente } from 'src/app/model/utente.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public registerFormModel: FormGroup;
  private registerTitle: string;
  private registerSubTitle: string;

  constructor(
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private navController: NavController,
    private utenteService: UtenteService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.registerFormModel = this.formBuilder.group({
      name_surname: ['mario rotolo', Validators.compose([
        Validators.required
      ])],
      email: ['comixgang@gmail.com', Validators.compose([
        Validators.required
      ])],
      phone: ['1234567890', Validators.compose([
        Validators.required
      ])],
      code: ['cuocodelcazzo', Validators.compose([
        Validators.required
      ])],
      password: ['ciaociaociao', Validators.compose([
        Validators.required
      ])],
      password_confirm: ['ciaociaociao', Validators.compose([
        Validators.required
      ])]
    });
  }

  async showRegisterError() {
    const alert = await this.alertController.create({
      header: this.registerTitle,
      message: this.registerSubTitle,
      buttons: ['OK']
    });

    await alert.present();
  }

  onRegister() {
    const account: Account = this.registerFormModel.value;
    console.error(account);
    this.utenteService.register(account).subscribe((utente: Utente) => {
      this.registerFormModel.reset();
      this.navController.navigateRoot('login');
    },
      (err: HttpErrorResponse) => {
        if (err.status === 401) {
          console.error('register request error: ' + err.status);
          this.showRegisterError();
        }
      });
  }

  // Dismiss Register Modal
  dismissRegister() {
    this.modalController.dismiss();
  }
  // On Login button tap, dismiss Register modal and open login Modal
  async loginModal() {
    this.dismissRegister();
    return await null;
  }
}
