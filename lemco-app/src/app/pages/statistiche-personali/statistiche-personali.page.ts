import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Corso } from 'src/app/model/corso.model';
import { CoursiService } from 'src/app/services/coursi.service';
import { Utente } from 'src/app/model/utente.model';
import { UtenteService } from 'src/app/services/utente.service';
import { RisposteStatistiche, CorsiStatistica } from 'src/app/model/risposta.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-statistiche-personali',
  templateUrl: './statistiche-personali.page.html',
  styleUrls: ['./statistiche-personali.page.scss'],
})
export class StatistichePersonaliPage implements OnInit {

  public corsi$: Observable<Corso[]>;
  public utente: Utente;
  public RisposteDiCorsi$: Observable<RisposteStatistiche>;
  public RisposteDiCorsiOb: CorsiStatistica[];

  constructor(
    private corsiService: CoursiService,
    private utenteService: UtenteService,
  ) { }

  ngOnInit() {
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
    });
    this.corsi$ = this.corsiService.listCorsi(this.utente.id);
    this.RisposteDiCorsi$ = this.corsiService.risposteDiCorsi(this.utente.id);
    this.RisposteDiCorsi$.subscribe((value) => {
      this.RisposteDiCorsiOb = value.courseList;
      console.log(this.RisposteDiCorsiOb[2]);
      console.log(value);
    });

  }

  getRisposte(id) {
    return this.RisposteDiCorsiOb[id];
  }
}
