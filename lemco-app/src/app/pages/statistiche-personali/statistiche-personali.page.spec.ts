import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistichePersonaliPage } from './statistiche-personali.page';

describe('StatistichePersonaliPage', () => {
  let component: StatistichePersonaliPage;
  let fixture: ComponentFixture<StatistichePersonaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatistichePersonaliPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistichePersonaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
