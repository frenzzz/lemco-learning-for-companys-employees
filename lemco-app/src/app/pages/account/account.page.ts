import { Component, OnInit } from '@angular/core';
import { UtenteService } from 'src/app/services/utente.service';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, } from '@angular/forms';
import { Utente } from 'src/app/model/utente.model';
import { Store } from 'src/app/model/store.model';
import { MESSAGE_RESPONSE, IMAGE_PROFILE } from 'src/app/constants';
import { Camera } from '@ionic-native/camera';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})

export class AccountPage implements OnInit {
  public accountFormModel: FormGroup;
  public utente: Utente;
  private store: Store;
  public isenabled: boolean = false;
  public imageSrc: string;
  public accountPage: boolean;

  constructor(
    private utenteService: UtenteService,
    private navController: NavController,
    private formBuilder: FormBuilder,
    private alertController: AlertController,

  ) { }

  ngOnInit() {
    //Utente
    this.utenteService.getUtente().subscribe((utente: Utente) => {
      this.utente = utente;
    });

    //Store
    this.utenteService.getStore().subscribe((store: Store) => {
      this.store = store;
    });

    //IMAGE
    this.imageSrc = this.utenteService.getImage();
    this.accountPage = true;

    //FORM
    if (this.utente.is_administrator == 1) {
      this.accountFormModel = this.formBuilder.group({
        name_surname: [this.utente.name_surname, Validators.compose([
          Validators.required
        ])],
        email: [this.utente.email, Validators.compose([
          Validators.required
        ])],
        phone: [this.utente.phone, Validators.compose([
          Validators.required
        ])],
        code: [this.store.code, Validators.compose([
          Validators.required
        ])],
        password: ['', Validators.compose([
          Validators.required
        ])],
        //solo admin
        name: [this.store.name, Validators.compose([
          Validators.required
        ])],
        street: [this.store.street, Validators.compose([
          Validators.required
        ])],
        city: [this.store.city, Validators.compose([
          Validators.required
        ])],
        cap: [this.store.cap, Validators.compose([
          Validators.required
        ])]
      });
    } else {
      this.accountFormModel = this.formBuilder.group({
        name_surname: [this.utente.name_surname, Validators.compose([
          Validators.required
        ])],
        email: [this.utente.email, Validators.compose([
          Validators.required
        ])],
        phone: [this.utente.phone, Validators.compose([
          Validators.required
        ])],
        code: [this.store.code, Validators.compose([
          Validators.required
        ])],
        password: ['', Validators.compose([
          Validators.required
        ])]
      });
    }
  }

  logout() {
    this.utenteService.logout();
    this.navController.navigateRoot('login');
  }

  async updateUser() {
    const account: Utente = this.accountFormModel.value;
    console.log(account);
    account.id = this.utente.id;
    this.utenteService.updateProfilo(account).subscribe((utente: Utente) => {
      this.message(utente[MESSAGE_RESPONSE]);
    });
  }

  openGallery(): void {
    let cameraOptions = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: Camera.DestinationType.DATA_URL,
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      allowEdit: true,
    }
    Camera.getPicture(cameraOptions)
      .then(imageData => {
        this.imageSrc = "data:image/jpeg;base64," + imageData;
        this.utenteService.setImage(this.imageSrc);
      },
        err => console.log(err)
      );
    //this.storage.setItem(IMAGE_PROFILE, this.imageSrc);
    //this.accountFormModel.setValue.

  }

  async message(messageResponse: string) {
    const alert = await this.alertController.create({
      header: 'Messaggio',
      message: messageResponse,
      buttons: [{ text: 'OK' }]
    });
    await alert.present();
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Modifica la Password',
      inputs: [
        {
          name: 'passwordCorrente',
          type: 'password',
          placeholder: 'Password Corrente'
        },
        {
          name: 'passwordNuova',
          type: 'password',
          placeholder: 'Password Nuova'
        },
        {
          name: 'passwordRipeti',
          type: 'password',
          placeholder: 'Ripeti Password'
        }
      ],
      buttons: [
        {
          text: 'Annulla',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Modifica',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

}
