import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, from } from 'rxjs';
import { Questionario } from 'src/app/model/questionario.model';
import { QuestionarioService } from 'src/app/services/questionario.service';
import { Domanda } from 'src/app/model/domanda.model';
import { DomandaService } from 'src/app/services/domanda.service';
import { Risposta } from 'src/app/model/risposta.model';
import { RispostaService } from 'src/app/services/risposta.service';
// import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtenteService } from 'src/app/services/utente.service';
import { Utente } from 'src/app/model/utente.model';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-questionario',
  templateUrl: './questionario.page.html',
  styleUrls: ['./questionario.page.scss'],
})

export class QuestionarioPage implements OnInit {

  @ViewChild(IonSlides, {static: false}) slides: IonSlides;

  constructor(
    private activatedRoute: ActivatedRoute,
    private questionarioService: QuestionarioService,
    private domandaService: DomandaService,
    private rispostaService: RispostaService,
    private utenteService: UtenteService,
  ) { }

  public slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  public id_questionnaire = null;
  public questionario$: Observable<Questionario>;
  public domande$: Observable<Domanda[]>;
  public risposte$: Observable<Risposta[]>;
  private utente: Utente;

  ngOnInit() {
    this.id_questionnaire = this.activatedRoute.snapshot.paramMap.get('id');
    this.questionario$ = this.questionarioService.singleQuestionnaire(this.id_questionnaire);
    this.domande$ = this.domandaService.domandeList(this.id_questionnaire);
    this.risposte$ = this.rispostaService.risposteListWithIdQuestionario(this.id_questionnaire);
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
    });
  }

  async mcqAnswer(questionID, answer) {
    return await this.rispostaService.salvaRisposta(this.utente.id, questionID, answer, this.id_questionnaire);
  }

  nextSlide() {
    this.slides.slideNext(1000);
  }

}
