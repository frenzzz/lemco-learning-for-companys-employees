import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController, ModalController } from '@ionic/angular';
import { UtenteService, Account } from 'src/app/services/utente.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utente } from 'src/app/model/utente.model';
import { RegisterPage } from '../register/register.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginFormModel: FormGroup;
  private loginTitle: string;
  private loginSubTitle: string;
  private utente: Utente;

  constructor(
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private navController: NavController,
    private utenteService: UtenteService,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.loginFormModel = this.formBuilder.group({
      email: ['comixgang@gmail.com', Validators.compose([
        Validators.required
      ])],
      password: ['ciaociaociao', Validators.compose([
        Validators.required
      ])]
    });
    this.utenteService.getUtente().subscribe((utente: Utente) => {
      this.utente = utente;
    });
  }

  onLogin() {
    const account: Account = this.loginFormModel.value;
    this.utenteService.login(account).subscribe((utente: Utente) => {
      this.loginFormModel.reset();

      //console.log("Utente: " + this.utente.is_administrator);
      if (this.utente.is_administrator == 1) {
        this.navController.navigateRoot('tabs-admin');
      } else {
        this.navController.navigateRoot('tabs-employee');
      }
    },
      (err: HttpErrorResponse) => {
        if (err.status === 401) {
          console.error('login request error: ' + err.status);
          this.showLoginError();
        }
      });
  }

  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }

  // On Register button tap, dismiss login modal and open register modal
  async registerModal() {
    this.dismissLogin();
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }

  async showLoginError() {
    const alert = await this.alertController.create({
      header: this.loginTitle,
      message: this.loginSubTitle,
      buttons: ['OK']
    });

    await alert.present();
  }
}
