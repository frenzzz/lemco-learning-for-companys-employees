import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { Corso } from 'src/app/model/corso.model';
import { CoursiService } from 'src/app/services/coursi.service';
import { Lezione } from 'src/app/model/lezione.model';
import { LezioneService } from 'src/app/services/lezione.service';
import { Questionario } from 'src/app/model/questionario.model';
import { QuestionarioService } from 'src/app/services/questionario.service';
import { Blocco } from 'src/app/model/blocco.model';
import { BloccoService } from 'src/app/services/blocco.service';
import { RispostaService } from 'src/app/services/risposta.service';
import { UtenteService } from 'src/app/services/utente.service';
import { delay } from 'q';

@Component({
  selector: 'app-dettaglio-corso',
  templateUrl: './dettaglio-corso.page.html',
  styleUrls: ['./dettaglio-corso.page.scss'],
})
export class DettaglioCorsoPage implements OnInit {

  public corso$: Observable<Corso>;
  public lezioni$: Observable<Lezione[]>;
  public questionari$: Observable<Questionario[]>;
  public blocchi$: Observable<Blocco[]>;
  public myId = null;
  public idUser;
  public array = new Array();

  constructor(
    private activatedRoute: ActivatedRoute,
    private corsoService: CoursiService,
    private lezioneService: LezioneService,
    private questionarioService: QuestionarioService,
    private blocchiService: BloccoService,
    private rispostaService: RispostaService,
    private utenteService: UtenteService,
  ) { }

  ngOnInit() {
    this.myId = this.activatedRoute.snapshot.paramMap.get('id');
    this.corso$ = this.corsoService.findById(parseInt(this.myId));
    this.blocchi$ = this.blocchiService.listBlocchi(this.myId);
    this.lezioni$ = this.lezioneService.listLezioni(this.myId);
    this.questionari$ = this.questionarioService.listQuestionari(this.myId);
    this.utenteService.getUtente().subscribe((utente) => {
      this.idUser = utente.id;
    });
    this.arrayForQuestBlock()
  }

  async arrayForQuestBlock() {
    this.questionari$.subscribe((quest) => {
      quest.forEach(questi => {
        this.rispostaService.completeQuestionario(questi.id, this.idUser).subscribe((te) => {
          this.array[Number.parseInt(questi.id + '')] = te;
        })
      });
    });
  }

  doRefresh(event) {
    this.corso$ = this.corsoService.findById(parseInt(this.myId));
    this.blocchi$ = this.blocchiService.listBlocchi(this.myId);
    this.lezioni$ = this.lezioneService.listLezioni(this.myId);
    this.questionari$ = this.questionarioService.listQuestionari(this.myId);
    this.utenteService.getUtente().subscribe((utente) => {
      this.idUser = utente.id;
    });
    this.arrayForQuestBlock()
    event.target.complete();
  }

  isCompletedQuestionnaire(idQuestionario) {
    if (this.array[idQuestionario].completo) return true;
    else return false;
  }
}
