import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DettaglioCorsoPage } from './dettaglio-corso.page';

const routes: Routes = [
  {
    path: '',
    component: DettaglioCorsoPage
  },
  {
    path: 'lezione',
    loadChildren: '../../pages/lezione/lezione.module#LezionePageModule'
  },
  {
    path: 'questionario',
    loadChildren: '../../pages/questionario/questionario.module#QuestionarioPageModule'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DettaglioCorsoPage]
})
export class DettaglioCorsoPageModule {}
