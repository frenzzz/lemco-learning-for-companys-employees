import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IMieiCorsiPage } from './i-miei-corsi.page';

describe('IMieiCorsiPage', () => {
  let component: IMieiCorsiPage;
  let fixture: ComponentFixture<IMieiCorsiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IMieiCorsiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IMieiCorsiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
