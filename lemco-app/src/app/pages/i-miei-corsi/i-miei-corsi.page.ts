import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Corso } from 'src/app/model/corso.model';
import { CoursiService } from 'src/app/services/coursi.service';
import { Utente } from 'src/app/model/utente.model';
import { UtenteService } from 'src/app/services/utente.service';

@Component({
  selector: 'app-i-miei-corsi',
  templateUrl: './i-miei-corsi.page.html',
  styleUrls: ['./i-miei-corsi.page.scss'],
})
export class IMieiCorsiPage implements OnInit {

  public corsi$: Observable<Corso[]>;
  public utente: Utente;

  constructor(
    private corsiService: CoursiService,
    private utenteService: UtenteService,

  ) { }

  ngOnInit() {
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
    });
    this.corsi$ = this.corsiService.listCorsi(this.utente.id);
  }

  doRefresh(event) {
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
    });
    this.corsi$ = this.corsiService.listCorsi(this.utente.id);
    event.target.complete();
  }

}
