import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneCorsiPage } from './gestione-corsi.page';

describe('GestioneCorsiPage', () => {
  let component: GestioneCorsiPage;
  let fixture: ComponentFixture<GestioneCorsiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestioneCorsiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneCorsiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
