import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { GestioneCorsiPage } from './gestione-corsi.page';

const routes: Routes = [
  {
    path: '',
    component: GestioneCorsiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestioneCorsiPage]
})
export class GestioneCorsiPageModule {}
