import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AlertInput, AlertButton } from '@ionic/core';
import { Observable } from 'rxjs';

import { Utente } from 'src/app/model/utente.model';
import { UtenteService } from 'src/app/services/utente.service';
import { RuoloService } from 'src/app/services/ruolo.service';
import { RuoloAziendale } from 'src/app/model/ruolo_aziendale.model';
import { CoursiService } from 'src/app/services/coursi.service';
import { Corso } from 'src/app/model/corso.model';

@Component({
  selector: 'app-gestione-corsi',
  templateUrl: './gestione-corsi.page.html',
  styleUrls: ['./gestione-corsi.page.scss'],
})
export class GestioneCorsiPage implements OnInit {

  public ruoli$: Observable<RuoloAziendale[]>;
  public corsi$: Observable<Corso[]>;
  public corsi: Corso[];
  public utente: Utente;
  testCheckboxOpen: boolean;
  testCheckboxResult;
  public ruoliCorsoJSON: Object;

  constructor(
    private ruoloService: RuoloService,
    private utenteService: UtenteService,
    private corsoService: CoursiService,
    public alertCtrlCorsi: AlertController,
  ) { }

  ngOnInit() {
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
      this.ruoli$ = this.ruoloService.ruoliListWithIdAzienda(this.utente.id_store);
      this.corsi$ = this.corsoService.listCorsi(this.utente.id);
      console.log(this.utente.id);
    });

    this.ruoloService.tuttiRuoliCorsiJSON(this.utente.id_store).subscribe((cors) => {
      this.ruoliCorsoJSON = cors;
    });

    this.corsi$.subscribe((corsi) => {
      console.log(corsi);
      this.corsi = corsi;
    });
  }

  doRefresh(event) {
    this.ruoloService.tuttiRuoliCorsiJSON(this.utente.id_store).subscribe((cors) => {
      this.ruoliCorsoJSON = cors;
      event.target.complete();
    });
  }

  async doCheckboxCorsi(id, nomeRuolo) {
    const arrayCorsi = this.arrayInput(id);
    let buttOpt: AlertButton[] = [
      {
        text: 'Cancella',
      },
      {
        text: 'Salva',
        handler: data => {
          this.ruoloService.nuoviRuoliCorsi(id, data);
          this.testCheckboxOpen = false;
          this.testCheckboxResult = data;
          this.updateJSON();
        }
      },
    ];
    const alertCorsi = await this.alertCtrlCorsi.create({
      header: nomeRuolo,
      message: 'Seleziona i dipendeti per questo ruolo.',
      buttons: buttOpt,
      inputs: arrayCorsi,
    });
    await alertCorsi.present();
  }

  updateJSON() {
    this.ruoloService.tuttiRuoliCorsiJSON(this.utente.id_store).subscribe((data) => {
      this.ruoliCorsoJSON = data;
    });
  }

  arrayInput(id): AlertInput[] {
    let output = new Array;
    this.corsi.forEach(element => {
      let idCorso = element.id;
      let checket: boolean = this.bolean(idCorso, id);
      output.push({
        type: 'checkbox',
        label: element.title,
        value: element.id,
        checked: checket
      });
    });
    return output;
  }

  bolean(idCorso, idrole) {
    if (this.ruoliCorsoJSON[idCorso] != null && this.ruoliCorsoJSON[idCorso].includes(idrole)) {
      return true;
    } else return false;
  }
}
