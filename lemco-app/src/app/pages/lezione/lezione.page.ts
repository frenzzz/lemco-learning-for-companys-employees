import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { Lezione } from 'src/app/model/lezione.model';
import { LezioneService } from 'src/app/services/lezione.service';
import { File } from 'src/app/model/file.model';
import { FileService } from 'src/app/services/file.service';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';

@Component({
  selector: 'app-lezione',
  templateUrl: './lezione.page.html',
  styleUrls: ['./lezione.page.scss'],
})

export class LezionePage implements OnInit {
  public id_lesson = null;
  public lezione$: Observable<Lezione>;
  public file$: Observable<File>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private lezioneService: LezioneService,
    private fileService: FileService,
    private youtube: YoutubeVideoPlayer,
  ) { }

  ngOnInit() {
    this.id_lesson = this.activatedRoute.snapshot.paramMap.get('id');
    this.lezione$ = this.lezioneService.singleLesson(this.id_lesson);
    this.file$ = this.fileService.fileWithIdLessons(this.id_lesson);
  }

  openVideo(id) {
    this.youtube.openVideo(id);
  }

}
