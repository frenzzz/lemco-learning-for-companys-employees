import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AlertInput, AlertButton } from '@ionic/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Observable } from 'rxjs';

import { RuoloService } from 'src/app/services/ruolo.service';
import { RuoloAziendale } from 'src/app/model/ruolo_aziendale.model';
import { Utente, } from 'src/app/model/utente.model';
import { UtenteService } from 'src/app/services/utente.service';
import { Store } from 'src/app/model/store.model';
import { MESSAGGIO_INVITO } from 'src/app/constants';

@Component({
  selector: 'app-gestione-dipendenti',
  templateUrl: './gestione-dipendenti.page.html',
  styleUrls: ['./gestione-dipendenti.page.scss'],
})
export class GestioneDipendentiPage implements OnInit {

  public ruoli$: Observable<RuoloAziendale[]>;
  public dipendeti$: Observable<Utente[]>;
  public dipendeti: Utente[];
  public utente: Utente;
  public store: Store;
  public utenteRuoloJSON: Object;
  testCheckboxOpen: boolean;
  testCheckboxResult;

  constructor(
    private ruoloService: RuoloService,
    private utenteService: UtenteService,
    public alertCtrl: AlertController,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    this.utenteService.getUtente().subscribe((utente) => {
      this.utente = utente;
      this.ruoli$ = this.ruoloService.ruoliListWithIdAzienda(this.utente.id_store);
      this.dipendeti$ = this.utenteService.dipendentiWithStore(this.utente.id_store);
      this.ruoloService.tuttiRuoliDipendentiJSON(this.utente.id_store).subscribe((data) => {
        this.utenteRuoloJSON = data;
      });

      this.utenteService.getStore().subscribe((store) => {
        this.store = store;
      })

      this.dipendeti$.subscribe((dip) => {
        this.dipendeti = dip;
      });

    });
  }

  doRefresh(event) {
    this.ruoloService.tuttiRuoliDipendentiJSON(this.utente.id_store).subscribe((data) => {
      this.utenteRuoloJSON = data;
      event.target.complete();
    });
  }

  async shareWhatsApp() {
    // Text + Image or URL works
    this.socialSharing.share(MESSAGGIO_INVITO + this.store.code, null, null).then(() => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  async doCheckbox(id, nomeRuolo) {
    const array = this.arrayInput(id);
    let buttOpt: AlertButton[] = [
      {
        text: 'Cancella',
      },
      {
        text: 'Salva',
        handler: data => {
          this.ruoloService.nuoviRuoliUtente(id, data)
          this.testCheckboxOpen = false;
          this.testCheckboxResult = data;
          this.updateJSON();
        }
      },
    ];
    const alert = await this.alertCtrl.create({
      header: nomeRuolo,
      message: 'Seleziona i dipendeti per questo ruolo.',
      buttons: buttOpt,
      inputs: array,
    });
    await alert.present();
  }

  updateJSON() {
    this.ruoloService.tuttiRuoliDipendentiJSON(this.utente.id_store).subscribe((data) => {
      this.utenteRuoloJSON = data;
    });
  }

  arrayInput(id): AlertInput[] {
    let output = new Array;
    this.dipendeti.forEach(element => {
      let idElement = element.id;
      let checket: boolean = this.bolean(idElement, id);
      output.push({
        type: 'checkbox',
        label: element.name_surname,
        value: element.id,
        checked: checket
      });
    });
    return output;
  }

  bolean(idElement, idrole) {
    if (this.utenteRuoloJSON[idElement] != null && this.utenteRuoloJSON[idElement].includes(idrole)) {
      return true;
    } else return false;
  }

}
