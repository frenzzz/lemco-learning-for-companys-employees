import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneDipendentiPage } from './gestione-dipendenti.page';

describe('GestioneDipendentiPage', () => {
  let component: GestioneDipendentiPage;
  let fixture: ComponentFixture<GestioneDipendentiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestioneDipendentiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneDipendentiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
