import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { GestioneDipendentiPage } from './gestione-dipendenti.page';

const routes: Routes = [
  {
    path: '',
    component: GestioneDipendentiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GestioneDipendentiPage]
})
export class GestioneDipendentiPageModule {}
