import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { AUTH_TOKEN, URL, UTENTE_STORAGE, X_AUTH, STORE_STORAGE, IMAGE_PROFILE } from '../constants';
import { Utente } from '../model/utente.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '../model/store.model';

export interface Account {
    name_surname: string;
    phone: string;
    email: string;
    password: string;
    password_confirm: string;
    code: string;
    is_administrator: boolean;

}

@Injectable({
    providedIn: 'root'
})
export class UtenteService {
    private authToken: string;
    private imageString: string;
    private loggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private utente$: BehaviorSubject<Utente> = new BehaviorSubject<Utente>({} as Utente);
    private store$: BehaviorSubject<Store> = new BehaviorSubject<Store>({} as Store);

    constructor(private http: HttpClient, private storage: Storage) {

        this.storage.get(AUTH_TOKEN).then((token) => {
            //console.log(token);
            this.authToken = token;
            if (token !== null && token !== undefined && token !== '') {
                this.loggedIn$.next(true);
            }
        });
        
        this.storage.get(UTENTE_STORAGE).then((utente) => {
            this.utente$.next(utente);
        });

        this.storage.get(STORE_STORAGE).then((store) => {
            this.store$.next(store);
        });

        this.storage.get(IMAGE_PROFILE).then((image) => {
            if(image != null && image != ''){
                this.imageString = image;
            } else {
                this.imageString = 'https://images.pexels.com/photos/2071882/pexels-photo-2071882.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260';
            }
        });
    }

    login(account: Account): Observable<Utente> {
        return this.http.post<Utente>(URL.LOGIN, { email: account.email, password: account.password }, { observe: 'response' }).pipe(
            map((resp: HttpResponse<Utente>) => {
                //const token = resp.headers.get(X_AUTH);
                const token = resp.body[X_AUTH];
                //console.log('TOKENN: ' + token);
                //console.log(token);
                this.storage.set(AUTH_TOKEN, token);
                this.authToken = token;
                // Utente memorizzato nello storage in modo tale che se si vuole cambiare il
                // profilo dell'utente stesso non si fa una chiamata REST.
                this.storage.set(UTENTE_STORAGE, resp.body[UTENTE_STORAGE]);
                this.storage.set(STORE_STORAGE, resp.body[STORE_STORAGE]);
                // update dell'observable dell'utente
                this.utente$.next(resp.body[UTENTE_STORAGE]);
                this.store$.next(resp.body[STORE_STORAGE]);
                this.loggedIn$.next(true);
                return resp.body[UTENTE_STORAGE];
            }));
    }

    register(account: Account): Observable<Utente> {
        return this.http.post<Utente>(URL.REGISTER, {
            name_surname: account.name_surname,
            email: account.email, phone: account.phone, code: account.code,
            password: account.password, password_confirm: account.password_confirm
        }, { observe: 'response' }).pipe(
            map((resp: HttpResponse<Utente>) => {
                const token = resp.body[X_AUTH];
                this.storage.set(AUTH_TOKEN, token);
                this.authToken = token;
                // Utente memorizzato nello storage in modo tale che se si vuole cambiare il
                // profilo dell'utente stesso non si fa una chiamata REST.
                this.storage.set(UTENTE_STORAGE, resp.body[UTENTE_STORAGE]);
                this.storage.set(STORE_STORAGE, resp.body[STORE_STORAGE]);
                // update dell'observable dell'utente
                this.utente$.next(resp.body[UTENTE_STORAGE]);
                this.loggedIn$.next(true);

                return resp.body;
            }));
    }

    logout() {
        this.authToken = null;
        this.loggedIn$.next(false);
        this.storage.remove(AUTH_TOKEN);
        this.storage.remove(UTENTE_STORAGE);
        this.storage.remove(STORE_STORAGE);
        // Nessuna chiamata al server perche' JWT e' stateless quindi non prevede alcun logout.
        // Per gestirlo si dovrebbe fare lato server una blacklist.
    }

    getUtente(): BehaviorSubject<Utente> {
        return this.utente$;
    }

    getStore(): BehaviorSubject<Store> {
        return this.store$;
    }

    getAuthToken(): string {
        return this.authToken;
    }

    isLogged(): Observable<boolean> {
        return this.loggedIn$.asObservable();
    }

    updateProfilo(nUtente: Utente): any {
        return this.http.put<Utente>(URL.UPDATE_PROFILO + '/' + nUtente.id, { nUtente }, { observe: 'response' }).pipe(
            map((resp: HttpResponse<Utente>) => {
                // Aggiornamento dell'utente nello storage.
                // Utente memorizzato nello storage per evitare chiamata REST quando si vuole modificare il profilo
                // e se l'utente chiude la app e la riapre i dati sono gia' presenti
                this.storage.set(UTENTE_STORAGE, resp.body[UTENTE_STORAGE]);
                // update dell'observable dell'utente
                this.utente$.next(resp.body[UTENTE_STORAGE]);
                return resp.body;
            }));
    }

    setImage(imageString: string) {
         this.storage.set(IMAGE_PROFILE, imageString);
    }

    getImage(): string {
        return this.imageString;
    }
   
    dipendentiWithStore(idStore): Observable<Utente[]> {
        const corsiUrl = `${URL.UTENTE_WITH_ID}/` + idStore;
        return this.http.get<Utente[]>(corsiUrl);
    }
}
