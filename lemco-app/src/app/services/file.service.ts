import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { File } from '../model/file.model'

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(
    private http: HttpClient
  ) { }

  fileWithIdLessons(id): Observable<File> {
    const corsiUrl = `${URL.FILE_WITH_ID}/` + id;
    return this.http.get<File>(corsiUrl);
  }
}
