import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { Domanda } from '../model/domanda.model';

@Injectable({
  providedIn: 'root'
})
export class DomandaService {

  constructor(
    private http: HttpClient
  ) { }

  domandeList(id): Observable<Domanda[]> {
    const corsiUrl = `${URL.DOMANDA_WITH_ID}/` + id;
    return this.http.get<Domanda[]>(corsiUrl);
  }
}
