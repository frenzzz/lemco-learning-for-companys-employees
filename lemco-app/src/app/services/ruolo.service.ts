import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { RuoloAziendale } from '../model/ruolo_aziendale.model';
import { URL } from '../constants';
import { Utente, UtenteRuolo } from '../model/utente.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RuoloService {


  constructor(
    private http: HttpClient,
  ) { }

  ruoliListWithIdAzienda(id): Observable<RuoloAziendale[]> {
    const corsiUrl = `${URL.RUOLO_WITH_ID}/` + id;
    return this.http.get<RuoloAziendale[]>(corsiUrl);
  }

  utenteIsRole(idUser, idRole): Observable<RuoloAziendale> {
    const corsiUrl = `${URL.UTENTE_RUOLO}/` + idUser + "-" + idRole;

    let dio = this.http.get<RuoloAziendale>(corsiUrl);
    return dio;
  }

  utenteRuoloWithStore(idStore): Observable<UtenteRuolo[]> {
    const corsiUrl = `${URL.UTENTE_RUOLO_WITH_STORE}/` + idStore;
    return this.http.get<UtenteRuolo[]>(corsiUrl);
  }

  tuttiRuoliDipendentiJSON(idStore) {
    const corsiUrl = `${URL.TUTTI_RUOLI_UTENTE}/` + idStore;
    return this.http.get(corsiUrl);
  }

  tuttiRuoliCorsiJSON(idStore) {
    const corsiUrl = `${URL.TUTTI_RUOLI_CORSO}/` + idStore;
    return this.http.get(corsiUrl);
  }

  nuoviRuoliUtente(idRuolo: any, data: any) {
    let out = null;
    this.http.post<any>(URL.RUOLO, {'id_ruolo': idRuolo, 'id_utenti': data}, { observe: 'response' })      
    .subscribe(data => {
      out = data.body;
    }, error => {
      console.log(error);
    });
    return out;
  }

  nuoviRuoliCorsi(idRuolo: any, data: any) {
    let out = null;
    this.http.post<any>(URL.CORSO, {'id_ruolo': idRuolo, 'id_corsi': data}, { observe: 'response' })      
    .subscribe(data => {
      out = data.body;
    }, error => {
      console.log(error);
    });
    return out;
  }
}
