import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { Questionario } from '../model/questionario.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionarioService {

  constructor(
    private http: HttpClient
  ) { }

  listQuestionari(id): Observable<Questionario[]> {
    const corsiUrl = `${URL.QUESTIONARIO_WITH_ID}/` + id;
    return this.http.get<Questionario[]>(corsiUrl);
  }
  
  singleQuestionnaire(id): Observable<Questionario> {
    const corsiUrl = `${URL.QUESTIONARIO}/` + id;
    return this.http.get<Questionario>(corsiUrl);
  }
}
