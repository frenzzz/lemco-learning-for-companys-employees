import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { Blocco } from '../model/blocco.model';


@Injectable({
  providedIn: 'root'
})
export class BloccoService {

  constructor(
    private http: HttpClient
  ) { }

  listBlocchi(id): Observable<Blocco[]> {
    const corsiUrl = `${URL.BLOCCO_WITH_ID}/` + id;
    return this.http.get<Blocco[]>(corsiUrl);
  }

}
