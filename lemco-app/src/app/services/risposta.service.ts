import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { Risposta, RisposteQuestionario } from '../model/risposta.model';

@Injectable({
  providedIn: 'root'
})
export class RispostaService {

  constructor(
    private http: HttpClient,
  ) { }

  risposteListWithIdQuestionario(id): Observable<Risposta[]> {
    const corsiUrl = `${URL.RISPOSTA_WITH_ID}/` + id;
    return this.http.get<Risposta[]>(corsiUrl);
  }

  completeQuestionario(idQuestionario, idUser): Observable<RisposteQuestionario> {
    const corsiUrl = `${URL.RISPOSTE_QUESTIONARIO}/` + idQuestionario + "-" + idUser;
    return this.http.get<RisposteQuestionario>(corsiUrl);
  }

  salvaRisposta(idUtente, questionID, answer, idQuestionario) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');

    let postData = {
      'id_utente': idUtente,
      'id_domanda': questionID['0'],
      'id_risposta': answer['0'],
      'id_questionario': idQuestionario
    };
    console.log(postData);

    return this.http.post(URL.RISPOSTA, postData, { observe: 'response' })
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);
      });
  }

}
