import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Corso } from '../model/corso.model';
import { URL } from '../constants';
import { RisposteStatistiche } from '../model/risposta.model';

@Injectable({
  providedIn: 'root'
})
export class CoursiService {

  constructor(
    private http: HttpClient,
  ) { }

  listCorsi(utente): Observable<Corso[]> {
    const corsiUrl = `${URL.CORSI_WITH_ID}/` + utente;
    return this.http.get<Corso[]>(corsiUrl);
  }

  findById(id_corso: number): Observable<Corso> {
    const apiURL = `${URL.CORSO}/${id_corso}`;
    return this.http.get<Corso>(apiURL);
  }

  risposteDiCorsi(idCorso: Uint8Array): Observable<RisposteStatistiche>{
    const apiURL = `${URL.RISPOSTE_CORSI}/${idCorso}`;
    return this.http.get<RisposteStatistiche>(apiURL);
  }
}
