import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { URL } from '../constants';
import { Lezione } from '../model/lezione.model';
import { UtenteService } from './utente.service';
import { Utente } from '../model/utente.model';

@Injectable({
  providedIn: 'root'
})
export class LezioneService {

  private utente: Utente;

  constructor(
    private http: HttpClient,
    private utenteService: UtenteService,
  ) {
    this.utenteService.getUtente().subscribe((ut) => {
      this.utente = ut;
    })
  }

  
  listLezioni(id): Observable<Lezione[]> {
    const corsiUrl = `${URL.LEZIONE_WITH_ID}/` + id;
    return this.http.get<Lezione[]>(corsiUrl);
  }
  
  singleLesson(id): Observable<Lezione> {
    const corsiUrl = `${URL.LEZIONE}/` + id + '-' + this.utente.id;
    return this.http.get<Lezione>(corsiUrl);
  }

}
