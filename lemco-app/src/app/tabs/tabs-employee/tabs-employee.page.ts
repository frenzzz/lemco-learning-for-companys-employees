import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Utente } from '../../model/utente.model';
import { UtenteService } from '../../services/utente.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tabs-employee',
  templateUrl: './tabs-employee.page.html',
  styleUrls: ['./tabs-employee.page.scss'],
})
export class TabsEmployeePage implements OnInit {

  public utente$: BehaviorSubject<Utente>;
  public imageSrc: string;

  constructor(
    private utenteService: UtenteService,
    private navController: NavController,
  ) { }

  ngOnInit() {
    this.utente$ = this.utenteService.getUtente();
    //IMAGE
    this.imageSrc = this.utenteService.getImage();
  }

  logout() {
    this.utenteService.logout();
    this.navController.navigateRoot('login');
  }

}
