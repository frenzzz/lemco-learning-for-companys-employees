import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsEmployeePage } from './tabs-employee.page';

const routes: Routes = [
  {
    path: '',
    component: TabsEmployeePage,
    children: [
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: '../../pages/account/account.module#AccountPageModule'
          }
        ]
      },
      {
        path: 'i-miei-corsi',
        children: [
          {
            path: '',
            loadChildren: '../../pages/i-miei-corsi/i-miei-corsi.module#IMieiCorsiPageModule',
          },
          {
            path: ':id',
            loadChildren: '../../pages/dettaglio-corso/dettaglio-corso.module#DettaglioCorsoPageModule'
          }
        ]
      },
      {
        path: 'statistiche-personali',
        children: [
          {
            path: '',
            loadChildren: '../../pages/statistiche-personali/statistiche-personali.module#StatistichePersonaliPageModule'
          },
        ]
      },

      {
        path: '',
        redirectTo: '/tabs-employee/i-miei-corsi',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsEmployeePage]
})
export class TabsEmployeePageModule { }
