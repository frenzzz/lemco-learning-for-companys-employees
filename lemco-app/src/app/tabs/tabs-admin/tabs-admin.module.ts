import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsAdminPage } from './tabs-admin.page';

const routes: Routes = [
  {
    path: '',
    component: TabsAdminPage,
    children: [
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: '../../pages/account/account.module#AccountPageModule'
          }
        ]
      },
      {
        path: 'gestione-dipendenti',
        children: [
          {
            path: '',
            loadChildren: '../../pages/gestione-dipendenti/gestione-dipendenti.module#GestioneDipendentiPageModule'
          }
        ]
      },
      {
        path: 'gestione-corsi',
        children: [
          {
            path: '',
            loadChildren: '../../pages/gestione-corsi/gestione-corsi.module#GestioneCorsiPageModule'
          }
        ]
      },
      {
        path: 'statistiche-personali',
        children: [
          {
            path: '',
            loadChildren: '../../pages/statistiche-personali/statistiche-personali.module#StatistichePersonaliPageModule'
          },
        ]
      },
      {
        path: '',
        redirectTo: '/tabs-admin/gestione-dipendenti',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsAdminPage]
})
export class TabsAdminPageModule { }
