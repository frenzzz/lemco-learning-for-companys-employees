import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Utente } from '../../model/utente.model';
import { UtenteService } from '../../services/utente.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tabs-admin',
  templateUrl: './tabs-admin.page.html',
  styleUrls: ['./tabs-admin.page.scss'],
})
export class TabsAdminPage implements OnInit {

  public utente$: BehaviorSubject<Utente>;
  public imageSrc: string;

  constructor(
    private utenteService: UtenteService,
    private navController: NavController,
  ) { }

  ngOnInit() {
    this.utente$ = this.utenteService.getUtente();
    this.imageSrc = this.utenteService.getImage();
  }

  logout() {
    this.utenteService.logout();
    this.navController.navigateRoot('login');
  }
}
